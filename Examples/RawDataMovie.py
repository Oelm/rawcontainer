#!/usr/bin/env python
"""
An animated image
"""

from RawContainer import RawContainerReader
import matplotlib
print matplotlib.get_backend()
# import matplotlib.rcsetup as rcsetup
#print(rcsetup.all_backends)
# matplotlib.use("TkAgg")  # this is for Ubuntu
import matplotlib.pyplot as plt
import matplotlib.animation as animation

filePath = "C://_Projects//PythonTasks//rawcontainer//Data//ScanRawData"
reader = RawContainerReader(filePath)
reader.open()
width = reader.width
height = reader.height

simpleImage = reader.readFrame(0) # single frame with number 0

plt.ion()  #Turns the interactive mode on.
fig = plt.figure()
im = plt.imshow(simpleImage, cmap='Greys')

def updatefig(*args):
    arry =reader.readNextFrame()
    if arry != None :
        im.set_array(arry)
    return im,

ani = animation.FuncAnimation(fig, updatefig, interval=70, blit=True)
plt.show(block=True)


reader.close()


